#include <stdio.h>
#include "numberToString.h"

int clear_stdin(void) {
    char c;
    do {
        c = (char) fgetc(stdin);
    } while (c != '\n' && c != EOF);
    return 1;
}


int main() {
    int number, numbers[10], length, negative;
    char string[256] = {0}, c;

    do {
        printf("Enter your number: ");
    } while ((scanf("%d%c", &number, &c) != 2 || c != '\n') && clear_stdin());

    numberToArray(number, numbers, &length, &negative);
    numberToText(numbers, string, length, negative);

    printf("Your number: %i\n"
           "It's long equivalent: %s\n", number, string);

    return 0;
}
