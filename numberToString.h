#ifndef NUMBER_TO_STRING
#define NUMBER_TO_STRING

// Функция для преобразования числа в массив цифр
// Ввод: число, указатель на массив цифр, указатель на переменную с длиной массива, указатель на бинарное значение для отрицательных чисел
// Вывод: -
void numberToArray(int number, int * numbers, int * length, int * negative);

// Функция для преобразования массива цифр в буквенный вариант числа
// Ввод: указатель на массив цифр, указатель на строку, куда записать результат, длина массива цифр, бинарное значение для отрицательных чисел
// Вывод: -
void numberToText(const int * numbers, char * string, int length, int negative);

int clear_stdin(void);

#endif