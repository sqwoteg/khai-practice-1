#include "numberToString.h"
#include <string.h>

void numberToText(const int * numbers, char * string, int length, int negative) {
    if (negative) {
        strcat(string, "minus ");
    }
    char quantities[10][10] = {
            "hundred ",
            "thousand ",
            "million "
    };
    char teens[10][10] = {
            "ten ",
            "eleven ",
            "twelve ",
            "thirteen ",
            "fourteen ",
            "fifteen ",
            "sixteen ",
            "seventeen ",
            "eighteen ",
            "nineteen "
    };
    char decades[9][10] = {
            "twenty ",
            "thirty ",
            "forty ",
            "fifty ",
            "sixty ",
            "seventy ",
            "eighty ",
            "ninety "
    };
    char digits[10][10] = {
            "zero ",
            "one ",
            "two ",
            "three ",
            "four ",
            "five ",
            "six ",
            "seven ",
            "eight ",
            "nine "
    };
    for (int i = length - 1; i >= 0; --i) {
        if (i > 0 && i % 3 == 1) {
            if (numbers[i] == 1) {
                strcat(string, teens[numbers[i - 1]]);
                --i;
                if (i == 3) {
                    strcat(string, quantities[1]);
                }
                if (i == 6) {
                    strcat(string, quantities[2]);
                }
            } else if (numbers[i] > 1) {
                strcat(string, decades[numbers[i] - 2]);
            }
        } else {
            if (numbers[i] > 0) {
                strcat(string, digits[numbers[i]]);
                if (i % 3 == 2) {
                    strcat(string, quantities[0]);
                }
            }
            if (i == 3 && !(numbers[3] == 0 && numbers[4] == 0 && numbers[5] == 0)) {
                strcat(string, quantities[1]);
            }
            if (i == 6) {
                strcat(string, quantities[2]);
            }
        }
    }
}

void numberToArray(int number, int * numbers, int * length, int * negative) {
    if (number < 0) {
        *negative = 1;
        number *= -1;
    } else {
        *negative = 0;
    }
    int i = 0;
    while (number != 0) {
        numbers[i] = number % 10;
        number /= 10;
        i++;
    }
    if (i == 0) {
        numbers[0] = 0;
        i++;
    }
    *length = i;
}